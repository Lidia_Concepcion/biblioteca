package com.techuniversity.biblioteca.utils;

public enum EstadosLibro {
    DISPONIBLE,
    PRESTADO,
    PERDIDO;
}
