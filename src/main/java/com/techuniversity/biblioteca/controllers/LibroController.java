package com.techuniversity.biblioteca.controllers;

import com.techuniversity.biblioteca.model.AutorModel;
import com.techuniversity.biblioteca.model.LibroModel;
import com.techuniversity.biblioteca.services.LibroService;
import com.techuniversity.biblioteca.utils.EstadosLibro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.techuniversity.biblioteca.components.JWTBuilder;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/libros")
public class LibroController {

    @Autowired
    private LibroService libroService;

    @Autowired
    JWTBuilder jwtBuilder;

    // Metodo para recuperar listado de libros (pruebas de validacion metodo correctas, conusltas, token  http codes correctos.(TEST REALIZADOS)
    @GetMapping(path = "/", headers = {"Authorization"})
    public  ResponseEntity <List<LibroModel>> getAllLibros(@RequestHeader ("Authorization") String token, @RequestParam(defaultValue = "0") String page){
        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
            int intPage = Integer.parseInt(page);
            libroService.paginadoLibros(intPage);
             List<LibroModel> libros = libroService.paginadoLibros(intPage);
            return new ResponseEntity <List<LibroModel>>(libros,HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity("No esta autorizado para realizar esta acción, por favor revise sus credenciales", HttpStatus.UNAUTHORIZED);
        }
    }

    // Metodo para recuperar token, excepciones y http codes correctos.(TEST REALIZADOS)
    @GetMapping("/generarToken")
    public ResponseEntity<String> tokenGet(@RequestParam(value = "nombre", defaultValue = "lmji") String name) {
        try {
            return new ResponseEntity(jwtBuilder.generarToken(name, "admin"),HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity("Error al generar sus credenciales", HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Metodo para recuperar un valores de las tablas, excepciones y http codes correctos.(TEST REALIZADOS)
    @GetMapping(path = "/listados", headers = {"Authorization"})
    public ResponseEntity <List<LibroModel>> getDisponibles(@RequestHeader ("Authorization") String token, @RequestParam(defaultValue = " ") String clave, @RequestParam(defaultValue = " ") String valor) {
        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
            List<LibroModel> libros = libroService.getFiltradosEstado(clave,valor);
            if (libros.isEmpty())
                return new ResponseEntity("No existen datos para el filtro indicado:" + clave +",  " + valor,HttpStatus.NOT_FOUND);

            return new ResponseEntity <List<LibroModel>>(libros,HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity("No esta autorizado para realizar esta acción, por favor revise sus credenciales", HttpStatus.UNAUTHORIZED);
        }
    }


    // Metodo para recuperar un libro en concreto, excepciones y http codes correctos.(TEST REALIZADOS)
    @GetMapping(path = "/{id}")
    public ResponseEntity<LibroModel> getLibro(@PathVariable String id) {
        LibroModel libroModelSal  = new LibroModel();
        Optional<LibroModel> libroModel = libroService.getLibro(id);

         if (libroModel.isPresent()) {
             return new ResponseEntity<LibroModel>(libroModel.get(), HttpStatus.ACCEPTED);
         } else {
             return new ResponseEntity("No existen datos para la consulta", HttpStatus.NOT_FOUND);
         }
    }

    // Metodo para añadir un nuevo libro, excepciones y http codes correctos.(TEST REALIZADOS)
    @PostMapping(path = "/", headers = {"Authorization"})
    public ResponseEntity<LibroModel> addLibro(@RequestHeader ("Authorization") String token, @RequestBody LibroModel nuevo) {

        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
            libroService.insertLibro(nuevo);
            return new ResponseEntity<>(nuevo, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity("No esta autorizado para realizar esta acción, por favor revise sus credenciales", HttpStatus.UNAUTHORIZED);
        }

    }

    // Metodo para actualiza un libro existente, excepciones y http codes correctos.(TEST REALIZADOS)
    @PutMapping(path = "/{id}", headers = {"Authorization"})
    public ResponseEntity<LibroModel> updateLibro(@RequestHeader ("Authorization") String token, @PathVariable String id, @RequestBody LibroModel libro) {

        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
            // si no envian el id de entrada en el libro, lo incluimos en con el de la ventana
            libro.setId(id);
            // si el campo de entrada del id del autor no esta informado lo inicializamos con valor 1
            if (libro.getAutor().getId() == null) {
                libro.getAutor().setId("1");
            }
            libroService.save(libro);
            return new ResponseEntity<>(libro, HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            return new ResponseEntity("No esta autorizado para realizar esta acción, por favor revise sus credenciales", HttpStatus.UNAUTHORIZED);
        }
    }

    // Metodo para actualiza un libro existente, excepciones y http codes correctos.(TEST REALIZADOS)
    @DeleteMapping(path ="/{id}", headers = {"Authorization"})
    public ResponseEntity<Boolean> deleteLibro(@RequestHeader ("Authorization") String token, @PathVariable String id) {
        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
            //si no existe el libro que queremos borrar, no damos error.
            if (libroService.getLibro(id).isPresent()) {
                libroService.deleteLibro(libroService.getLibro(id).get());
            }
            return new ResponseEntity<>(true,HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity("No esta autorizado para realizar esta acción, por favor revise sus credenciales", HttpStatus.UNAUTHORIZED);
        }
    }

    // Metodo para actualiza un libro existente a estado prestado, excepciones y http codes correctos.(TEST REALIZADOS)
    @PatchMapping(path = "/prestamo/{id}", headers = {"Authorization"})
    public ResponseEntity<String> prestarLibro(@RequestHeader ("Authorization") String token, @PathVariable String id){
        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
            LibroModel libro = libroService.updEstadoLibro(id, EstadosLibro.PRESTADO);
            if (libro != null){
                return new ResponseEntity<>( "Has prestado este libro con éxito.", HttpStatus.ACCEPTED);
            }else{
                return new ResponseEntity("El libro no está disponible.", HttpStatus.ACCEPTED);
            }
        } catch (Exception ex) {
            return new ResponseEntity("No esta autorizado para realizar esta acción, por favor revise sus credenciales", HttpStatus.UNAUTHORIZED);
        }
    }
    // Metodo para actualiza un libro existente a estado devuelto, excepciones y http codes correctos.(TEST REALIZADOS)
    @PatchMapping(path = "/devolucion/{id}")
    public ResponseEntity<String> devolverLibro(@RequestHeader ("Authorization") String token, @PathVariable String id) {

        String userId;
        try {
            userId = jwtBuilder.validarToken(token);
            LibroModel libro = libroService.updEstadoLibro(id, EstadosLibro.DISPONIBLE);
            if (libro != null) {
                return new ResponseEntity<>("Has devuelto este libro con éxito.", HttpStatus.ACCEPTED);
            } else {
                return new ResponseEntity("No se ha podido devolver el libro.", HttpStatus.EXPECTATION_FAILED);
            }
        } catch (Exception ex) {
            return new ResponseEntity("No esta autorizado para realizar esta acción, por favor revise sus credenciales", HttpStatus.UNAUTHORIZED);
        }
    }
}
