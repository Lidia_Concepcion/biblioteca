package com.techuniversity.biblioteca.model;



import com.techuniversity.biblioteca.utils.EstadosLibro;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "libros")
public class LibroModel {

    @Id
    @NotNull
    private String id;
    private String titulo;
    private AutorModel autor;
    private String fecha_publi;
    private String idioma;
    private String editorial;
    private EstadosLibro estado;


    public LibroModel() {
    }

    public LibroModel(String id, String titulo, AutorModel autor, String fecha_publi, String idioma, String editorial, EstadosLibro estado) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.fecha_publi = fecha_publi;
        this.idioma = idioma;
        this.editorial = editorial;
        this.estado = estado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public AutorModel getAutor() {
        return autor;
    }

    public void setAutor(AutorModel autor) {
        this.autor = autor;
    }

    public String getFecha_publi() {
        return fecha_publi;
    }

    public void setFecha_publi(String fecha_publi) {
        this.fecha_publi = fecha_publi;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public EstadosLibro getEstado() {
        return estado;
    }

    public void setEstado(EstadosLibro estado) {
        this.estado = estado;
    }
}
