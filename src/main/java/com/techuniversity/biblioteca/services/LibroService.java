package com.techuniversity.biblioteca.services;


import com.mongodb.client.*;
import com.techuniversity.biblioteca.model.LibroModel;
import com.techuniversity.biblioteca.repository.LibroDAO;
import com.techuniversity.biblioteca.utils.EstadosLibro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.bson.Document;
import org.springframework.stereotype.Service;

import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class LibroService {
    @Autowired
    LibroDAO libroDAO;

    private static int pageLength = 3;

    public List<LibroModel> getLibros(){
        return libroDAO.findAll();
    }

    public Optional<LibroModel> getLibro(String id){
        return libroDAO.findById(id);
    }

    public LibroModel insertLibro(LibroModel libro){
        return libroDAO.insert(libro);
    }

    public LibroModel save(LibroModel prod) {
        return libroDAO.save(prod);
    }

    public boolean deleteLibro(LibroModel libro){
        try{
            libroDAO.delete(libro);
            return true;
        }catch (Exception e) {
            e.getMessage();
            return false;
        }
    }

    public List<LibroModel> paginadoLibros (int page){
        Pageable pageable = PageRequest.of(page, 3);
        Page<LibroModel> pages = libroDAO.findAll(pageable);
        List<LibroModel> libros = pages.getContent();
        return libros;
    }

    public List<LibroModel> getFiltradosEstado (String clave, String valor){
        MongoCollection<Document> librosCollection = Services.getLibrosCollection();
        Document doc = new Document();
        doc.append(clave, valor);
        FindIterable<Document> iterDoc = librosCollection.find(doc);

        Iterator iterador = iterDoc.iterator();
        List lista = new ArrayList();
        while (iterador.hasNext()) {
            lista.add(iterador.next());
        }
        return lista;
    }

    public LibroModel updEstadoLibro(String id, EstadosLibro valor) {
        LibroModel libro = getLibro(id).get();
        if (libro.getEstado().equals(valor))
            return null;

        libro.setEstado(valor);
        return save(libro);
    }

}
