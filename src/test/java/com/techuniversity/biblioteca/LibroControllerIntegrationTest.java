package com.techuniversity.biblioteca;

import com.techuniversity.biblioteca.components.JWTBuilder;
import com.techuniversity.biblioteca.controllers.LibroController;
import com.techuniversity.biblioteca.model.AutorModel;
import com.techuniversity.biblioteca.model.LibroModel;
import com.techuniversity.biblioteca.utils.EstadosLibro;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
public class LibroControllerIntegrationTest {

    @Test
    void contextLoads() {
    }

    @Autowired
    LibroController libroController;

    @Autowired
    JWTBuilder jwtBuilder;

    private String getToken() {
        return jwtBuilder.generarToken("Tester", "admin");
    }

    @Test
    @Order(1)
    public void testAddLibro() {
        System.out.println("UNO");

        AutorModel autor = new AutorModel();
        autor.setId("null");
        LibroModel libro = new LibroModel("createInTest", "Test", autor, "0000", "ES", "Planeta", EstadosLibro.DISPONIBLE);
        String token = getToken();

        ResponseEntity<LibroModel> reLibro = libroController.addLibro(token, libro);
        System.out.println(reLibro.getBody());
        System.out.println(reLibro.getBody().getId());
        assertEquals("createInTest", reLibro.getBody().getId());

    }

    @Test
    @Order(2)
    public void testGetLibro() {

        ResponseEntity<LibroModel> libro = libroController.getLibro("createInTest");
        assertEquals("createInTest", libro.getBody().getId());
        assertEquals("Test", libro.getBody().getTitulo());
    }

    @Test
    @Order(3)
    public void testPrestarLibro() {
        String token = getToken();
        ResponseEntity<String> mensaje = libroController.prestarLibro(token, "createInTest");
        System.out.println("PRESTADO: " + mensaje.getBody());
        assertEquals("Has prestado este libro con éxito.", mensaje.getBody());

    }

    @Test
    @Order(4)
    public void testPrestarLibroNoDisponible() {
        String token = getToken();
        ResponseEntity<String> mensaje = libroController.prestarLibro(token, "createInTest");
        System.out.println(mensaje.getBody());
        assertEquals("El libro no está disponible.", mensaje.getBody());

    }

    @Test
    @Order(5)
    public void testDevolverLibro() {
        String token = getToken();
        ResponseEntity<String> mensaje = libroController.devolverLibro(token, "createInTest");
        System.out.println("PRESTADO: " + mensaje.getBody());
        assertEquals("Has devuelto este libro con éxito.", mensaje.getBody());

    }

    @Test
    @Order(6)
    public void testDevolverLibroNoDisponible() {
        String token = getToken();
        ResponseEntity<String> mensaje = libroController.devolverLibro(token, "createInTest");
        System.out.println(mensaje.getBody());
        assertEquals("No se ha podido devolver el libro.", mensaje.getBody());

    }


    @Test
    @Order(7)
    public void testUpdateLibro() {
        System.out.println("DOS");
        AutorModel autor = new AutorModel();
        autor.setId("null");
        LibroModel libro = new LibroModel("createInTest", "Test Updated", autor, "0000", "ES", "Planeta", EstadosLibro.DISPONIBLE);
        String token = getToken();

        ResponseEntity<LibroModel> updLibro = libroController.updateLibro(token, "createInTest", libro);

        System.out.println(updLibro.getBody().getTitulo());
        assertEquals("Test Updated", updLibro.getBody().getTitulo());
        libro.setTitulo("Test");
        ResponseEntity<LibroModel> updLibro2 = libroController.updateLibro(token, "createInTest", libro);
        assertEquals("Test", updLibro.getBody().getTitulo());

    }

    @Test
    @Order(8)
    public void testDeleteLibro() {
        System.out.println("TRES");
        String token = getToken();
        ResponseEntity<Boolean> deleteLibro = libroController.deleteLibro(token, "createInTest");
        assertTrue(deleteLibro.getBody());

    }

}
